package io.gitlab.mihajlonesic.jsf.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class CounterThree {
    private int value = 0;

    public CounterThree() {}

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    public String increment() {
        this.value++;
        return "scope-request";
    }
}