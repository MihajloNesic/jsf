package io.gitlab.mihajlonesic.jsf.managedbeans;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class StudentData {
    private List<Student> students;

    public StudentData() {
        loadSampleData();
    }

    public List<Student> getStudents() {
        return students;
    }
    
    private void loadSampleData() {
        students = new ArrayList<>();
        
        students.add(new Student("John", "Doe"));
        students.add(new Student("Mary", "Public"));
        students.add(new Student("Jimmy", "Smith"));
    }
}