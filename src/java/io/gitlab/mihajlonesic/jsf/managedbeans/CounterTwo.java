package io.gitlab.mihajlonesic.jsf.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class CounterTwo {
    private int value = 0;

    public CounterTwo() {}

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    public String increment() {
        this.value++;
        return "scope-session";
    }
}