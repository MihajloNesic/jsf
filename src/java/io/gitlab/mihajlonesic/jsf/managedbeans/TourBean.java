package io.gitlab.mihajlonesic.jsf.managedbeans;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class TourBean {
    
    private String kindOfTour;

    public TourBean() {}

    public String getKindOfTour() {
        return kindOfTour;
    }

    public void setKindOfTour(String kindOfTour) {
        this.kindOfTour = kindOfTour;
    }
    
    public String startTheTour() {
        switch(kindOfTour) {
            case "city":
                return "cn-tour-city";
            case "country":
                return "cn-tour-country";    
            default: 
                return "cn-tour-country";
        }
    }
}