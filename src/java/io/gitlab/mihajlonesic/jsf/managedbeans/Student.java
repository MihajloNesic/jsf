package io.gitlab.mihajlonesic.jsf.managedbeans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;

/*
    Managed Bean:
    1) public no-arg constructor
    2) expose properties via public getter/setter methods 
*/

@ManagedBean
public class Student {
    public String firstName;
    public String lastName;
    public String country;
    public String department;
    public List<String> favLanguages;

    List<String> countryOptions;
    
    public Student() {
        countryOptions = new ArrayList<>();
        
        countryOptions.add("Brazil");
        countryOptions.add("France");
        countryOptions.add("Spain");
        countryOptions.add("Japan");
        countryOptions.add("Russia");
        countryOptions.add("Serbia");
        
        // pre-populate the bean
        this.firstName = "John";
        this.lastName = "Doe";
        this.country = "Brazil";
        this.department = "SE";
        this.favLanguages = Arrays.asList("Java", "C#", "Perl");
    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public List<String> getCountryOptions() {
        return countryOptions;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public List<String> getFavLanguages() {
        return favLanguages;
    }

    public void setFavLanguages(List<String> favLanguages) {
        this.favLanguages = favLanguages;
    }
}